
#!/bin/bash


PID=$(ps -ef |grep openvpn | grep -v grep | awk '{print $2}'|awk -F '/' '{print $1}')

if [ -n "$PID" ]; then
        kill -9 $PID
        echo kill $PID successful
else
        echo jar is not running $PID
fi
